#!/bin/sh
############################################################################
##
## Copyright (C) 2021 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

# Checkout latest revision of all or selected meta layers and update the sha1s to the manifest.xml

MANIFEST=$(dirname $(realpath $0))/manifest.xml

repo sync $@ -n
repo forall $@ -c "\
 OLD_REV=\$(grep -A2 \${REPO_PROJECT} ${MANIFEST} | grep revision | sed -e 's/.*\"\(.*\)\"/\1/') ; \
 git checkout \$REPO_REMOTE/\$REPO_UPSTREAM ; \
 echo Changelog for \$REPO_PROJECT: ; \
 git log --pretty=format:'%h %s' --abbrev-commit \${OLD_REV}..HEAD ; \
 sed -i -e /\${REPO_PROJECT}/,/path/s/revision.*/revision=\\\"\$(git rev-parse HEAD)\\\"/ ${MANIFEST}"
